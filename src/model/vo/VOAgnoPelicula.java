package model.vo;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;

public class VOAgnoPelicula {

	private int agno;
	private ILista<VOPelicula> peliculas;
	
	public VOAgnoPelicula(){
		peliculas = new ListaEncadenada<VOPelicula>();
	}
	public int getAgno() {
		return agno;
	}
	public void setAgno(int agno) {
		this.agno = agno;
	}
	public ILista<VOPelicula> getPeliculas() {
		return peliculas;
	}
	public void setPeliculas(VOPelicula pelicula) {
		peliculas.agregarElementoFinal(pelicula);
	}
	
	
}
