package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;



import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;


	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {

		misPeliculas = new ListaEncadenada<VOPelicula>();
		peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();
		BufferedReader br=null;

		try {
			br=new BufferedReader(new FileReader("data/movies.csv"));
			String linea = br.readLine();
			linea = br.readLine();
			while(linea != null ){

				misPeliculas.agregarElementoFinal(VOcrearPelicula(linea));
				linea = br.readLine();
			}
			organizarLista();
			System.out.println("----------------------SE HAN CARGADO LAS PELICULAS CORRECTAMENTE----------------------");
			br.close();
		} catch (Exception e) {			
			e.printStackTrace();
		}

	}

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {

		ListaEncadenada<VOPelicula> temp = new ListaEncadenada<VOPelicula>();
		for(VOPelicula a: misPeliculas)
		{
			if(a.getTitulo().contains(busqueda))
			{
				temp.agregarElementoFinal(a);
			}
		}
		return temp;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {
		int contador = 0;
		for(VOAgnoPelicula x: peliculasAgno)
		{

			if(x.getAgno() == agno)
			{
				peliculasAgno.darElemento(contador);
				return x.getPeliculas();
			}
			contador ++;
		}

		return null;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		peliculasAgno.avanzarSiguientePosicion();
		return peliculasAgno.darElementoPosicionActual();

	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		peliculasAgno.retrocederPosicionAnterior();
		return peliculasAgno.darElementoPosicionActual();
	}

	public VOPelicula VOcrearPelicula(String a)
	{

		VOPelicula nueva = new VOPelicula();

		// AGREGA LA LISTA DE GENEROS A LA PELICULA
		int posultima = a.lastIndexOf(",");
		String palabra = a.substring(posultima+1);
		ListaEncadenada<String> generos = new ListaEncadenada<>();
		String[] generosArray =palabra.split("\\|");
		for(int i=0; i < generosArray.length; i++ )
		{
			generos.agregarElementoFinal(generosArray[i]);
		}
		nueva.setGenerosAsociados(generos);

		//AGREGA EL NOMBRE

		int posprimera = a.indexOf(",");
		String nombre;
		if (a.contains("\""))
		{
			nombre = (String) a.subSequence(posprimera+1, posultima);
		}else 
		{
			nombre = (String) a.subSequence(posprimera+1, posultima-6);

		}
		nueva.setTitulo(nombre);


		//AGREGA EL A�IO
		String sinGeneros = (String) a.subSequence(0, posultima);
		int posanio = sinGeneros.lastIndexOf("(");
		if(posanio != -1)
		{
			String anio = sinGeneros.replaceAll("\""," " ).trim().substring(posanio+1,posanio+5);

			nueva.setAgnoPublicacion(Integer.parseInt(anio));
		}else
		{
			nueva.setAgnoPublicacion(0000);
		}




		// REVISA SI ESTA EL A�O EN LA LISTA peliculasAgno

		Boolean EstaAnio = false;
		for(VOAgnoPelicula listaAno : peliculasAgno)
		{
			int aniolista =listaAno.getAgno();
			if(aniolista == nueva.getAgnoPublicacion())
			{
				listaAno.setPeliculas(nueva);
				EstaAnio = true;
			}
		}
		if(EstaAnio == false)
		{
			VOAgnoPelicula temp = new VOAgnoPelicula();
			temp.setAgno(nueva.getAgnoPublicacion());
			temp.setPeliculas(nueva);
			peliculasAgno.agregarElementoFinal(temp);
		}


		System.out.println("Nombre:"+nueva.getTitulo() +" a�o: " + nueva.getAgnoPublicacion() +" generos: "+ Arrays.toString(generosArray));
		return nueva;

	}

	public void organizarLista ()
	{
		// ORGANIZA NUMERICAMENTE LA LISTA

		VOAgnoPelicula [] temp = new VOAgnoPelicula [peliculasAgno.darNumeroElementos()];

		for (int i = 0; i < peliculasAgno.darNumeroElementos(); i++) {
			temp[i] = peliculasAgno.darElemento(i);
		}

		int N = temp.length;
		for (int i = 1; i < N; i++) {
			VOAgnoPelicula porIns = temp[i];
			boolean termino = false;
			for (int j = i; j >0 && !termino; j--) {
				VOAgnoPelicula actual = temp[j-1];
				if(actual.getAgno() > porIns.getAgno())
				{
					temp[j] = actual;
					temp[j-1] = porIns;
				}else
				{
					termino = true;
				}
			}
		}
		peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();
		for (int i = 0; i < temp.length; i++) {
			peliculasAgno.agregarElementoFinal(temp[i]);
		}



	}

}
