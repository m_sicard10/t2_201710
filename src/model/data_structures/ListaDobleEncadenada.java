package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoble<T> primero;
	private NodoDoble<T> actual;
	private NodoDoble<T> ultimo;




	private int totalNodos;

	public ListaDobleEncadenada(){
		primero = null;
		actual = primero;
		ultimo = null;
		totalNodos = 0;
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			private NodoDoble<T> actualT=null;
			public boolean hasNext() {
				if(actualT==null)return primero!=null;
				else return actualT.getNextNode() != null;
			}

			public T next() {
				if(actualT==null){
					actualT=primero;
					if(actualT==null)return null;
					else return actualT.getItem();
				}
				else{
					actualT = actualT.getNextNode();
					return actualT.getItem();
				}

			}

			@Override
			public void remove() {
				// NA

			}
		};

	}

	@Override
	public void agregarElementoFinal(T elem) {
		NodoDoble<T> nuevoUlt = new NodoDoble<T>();
		nuevoUlt.setItem(elem);
		nuevoUlt.setNextNode(null);
		if(primero==null){
			primero = nuevoUlt;
			ultimo = nuevoUlt;
		}
		else{
			nuevoUlt.setPreviousNode(ultimo);
			ultimo.setNextNode(nuevoUlt);
			ultimo = nuevoUlt;
		}
		totalNodos++;
		actual = primero;
	}

	@Override
	public T darElemento(int pos) {
		actual=primero;
		if(actual!=null){	
			for (int i = 0; i < pos; i++) {
				actual = actual.getNextNode();
			}
			return actual.getItem();
		}
		return null;
	}


	@Override
	public int darNumeroElementos() {
		return totalNodos;
	}

	@Override
	public T darElementoPosicionActual() {
		if(actual!=null)return actual.getItem();
		else return null;
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		if(actual!=null&&actual.nextNode!= null){
			actual = actual.getNextNode();
			return true;
		}
		return false;

	}

	@Override
	public boolean retrocederPosicionAnterior() {
		if(actual!=null&&actual.previousNode!= null){
			actual = actual.getPreviousNode();
			return true;
		}
		return false;
	}

	static class NodoDoble <T>{
		private T item;

		private NodoDoble<T> nextNode;

		private NodoDoble<T> previousNode;

		public NodoDoble()
		{
			nextNode = null; 
			previousNode = null;
		}

		public T getItem() {
			return item;
		}

		public void setItem(T item) {
			this.item = item;
		}

		public NodoDoble<T> getNextNode() {
			return nextNode;
		}

		public void setNextNode(NodoDoble<T> nextNode) {
			this.nextNode = nextNode;
		}

		public NodoDoble<T> getPreviousNode() {
			return previousNode;
		}

		public void setPreviousNode(NodoDoble<T> previousNode) {
			this.previousNode = previousNode;
		}
	}

}
